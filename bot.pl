use strict;
use warnings;

use AnyEvent::Twitter::Stream;
use Net::Twitter;
use YAML::Tiny;
use JSON;
use Encode;

require './weather.pl';

my $cv = AnyEvent->condvar;

# include config.
my $config = (YAML::Tiny->read('config.yml'))->[0];
my $consumer_key = $config->{'consumer_key'};
my $consumer_secret = $config->{'consumer_secret'};
my $token = $config->{'token'};
my $token_secret = $config->{'token_secret'};
my $user_name = $config->{'user_name'};

# set Twitter config.
my $twitty = Net::Twitter->new(
  traits   => [qw/API::RESTv1_1/],
  consumer_key        => $consumer_key,
  consumer_secret     => $consumer_secret,
  access_token        => $token,
  access_token_secret => $token_secret,
);

# create condition variable.
my $done = AnyEvent->condvar;

# set key:name, val:num hash.
my %city_num = city_hash();

my $listener = AnyEvent::Twitter::Stream->new(
  token           => $token,
  token_secret    => $token_secret,
  consumer_key    => $consumer_key,
  consumer_secret => $consumer_secret,
  method          => "userstream",
  on_tweet => sub {
    my ($tweet) = @_;
    my $mention = $tweet->{entities}{user_mentions}[0];

    # if tweet addressed to myself.
    if ($mention->{screen_name} eq $user_name) {
      my $city = encode('utf-8', $tweet->{text});

      # using regular expressions to extract city name in tweet.
      my @regex_tweet = $city =~ /@.*\s.*(今日|明日)の(.*)の天気.*/g;
      my $decode_tweet = decode('utf-8', $regex_tweet[0]);
      my $num;
      my $d = "今日";
      my $decoder = decode('utf-8', $d);

      if ($decode_tweet eq $decoder) {
        $num = 0;
      }
      else {
        $num = 1;
      }

      # get weather info.
      for (@regex_tweet) {
        if (exists($city_num{$_})) {
          my $sentence = get_weather($city_num{$_}, $num);
          my $reply = decode('utf-8',$sentence);
          my $sender = $tweet->{user}{screen_name};
          my $res = "\@$sender $reply";

          $twitty->update($res);
        }
      }
    }
  },
    on_error => sub {
        my $error = shift;
        warn "ERROR: $error";
        $done->send;
    },
    on_eof   => sub {
        $done->send;
    },
);

$done->recv;
