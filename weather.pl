use strict;
use warnings;

use LWP::Simple;
use Encode;
use Jcode;
use JSON;

# make hash.
# 'city name' is the key and 'city number' is the associated value.
sub city_hash {
  my $url = "http://weather.livedoor.com/forecast/rss/primary_area.xml";

  my $response = get($url);

  my @nums = $response =~ /id="([0-9]*)/g;
  my @titles = $response =~ /city title="(\p{Han}+|\p{Hiragana}*)/g;

  my @encoded_title;

  for my $title (@titles) {
    push  @encoded_title, encode('utf-8',$title);
  }

  my %hash;

  @hash{@encoded_title} = @nums;

  return %hash;
}

# get weather info from number of argument.
sub get_weather {
  my $num = shift;
  my $url = "http://weather.livedoor.com/forecast/webservice/json/v1?city=$num";
  # today num(0) or tomorrow num(1)
  my $n = shift;

  my $get = get($url);
  my $json = decode_json($get);
  my $title = encode("utf-8", $json->{title});
  my $today_or_tomorrow  = encode("utf-8", $json->{forecasts}[$n]->{dateLabel});
  my $max = encode("utf-8", $json->{forecasts}[$n]->{temperature}->{max}->{celsius}); unless (defined $max) {$max = "わかんない";};
  my $min = encode("utf-8", $json->{forecasts}[$n]->{temperature}->{min}->{celsius}); unless (defined $min) {$min = "わかんない";};
  my $link = $json->{link};
  my $weather_info = "$today_or_tomorrowの$titleは最高気温$max最低気温$min\n";

  return $weather_info;
}

1;
